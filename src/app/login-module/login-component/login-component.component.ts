import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login-component',
  templateUrl: './login-component.component.html',
  styleUrls: ['./login-component.component.css']
})
export class LoginComponentComponent implements OnInit {

  constructor(private router: Router,
    private toastr: ToastrService) { }
  hide = true;
  objLog: any = {};
  ngOnInit() {

  }
  Login(isFlag: boolean, obj: any) {
    
    if (isFlag) {
      this.router.navigate(['pages']);
    } else {
      this.toastr.warning('Please enter requird fields');
    }
  }
}
