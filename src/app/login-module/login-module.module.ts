import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { LoginModuleRoutingModule } from './login-module-routing.module';
import { LoginComponentComponent } from './login-component/login-component.component';
//import { SharedModuleModule } from '../GlobalFolder/shared-module/shared-module.module';


@NgModule({
  declarations: [LoginComponentComponent],
  imports: [
    CommonModule,
    FormsModule,
    LoginModuleRoutingModule
   // SharedModuleModule
  ]
})
export class LoginModuleModule { }
