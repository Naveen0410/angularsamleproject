import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [

  {
    path: "login",
    loadChildren: () => import('./login-module/login-module.module').
      then(m => m.LoginModuleModule)
  },
  {
    path: "pages",
    loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule)
  },
  { path: "", redirectTo: "login", pathMatch: "full" }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
