import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import { HeaderComponentComponent } from 'src/app/Layout/header-component/header-component.component';
import { SiderComponentComponent } from 'src/app/Layout/sider-component/sider-component.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    HeaderComponentComponent,
    SiderComponentComponent
  ],
  exports: [
    HeaderComponentComponent,
    SiderComponentComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule
  ] 
})
export class SharedModuleModule { }
