import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages-component.component';
import { PageOneComponentComponent } from './page-one-component/page-one-component.component';
import { PageTwoComponentComponent } from './page-two-component/page-two-component.component';


const routes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {
        path: 'page1',
        component: PageOneComponentComponent
      }, {
        path: 'page2',
        component: PageTwoComponentComponent
      }, {
        path: '',
        redirectTo: 'page1',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule { }
