import { Component, OnInit, SimpleChanges, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-page-one-component',
  templateUrl: './page-one-component.component.html',
  styleUrls: ['./page-one-component.component.css']
})
export class PageOneComponentComponent implements OnInit {

  // name:any = new FormControl();

  /**  objEmp: any = new FormGroup({
 
     name: new FormControl(),
     deptment: new FormControl(),
     floor: new FormControl(),
     subject: new FormControl()
 
   });
 */

  isFlag: boolean = true;
  getClass() {
    return { "marginClas textColor": this.isFlag }
  }
  constructor(private fb: FormBuilder,
    private _toaster: ToastrService) { }

  ngAfterViewInit() {
    debugger
    console.log(">>>>> View Init >>>>>>>>>")
    //  throw new Error("Method not implemented.");
  }

  objEmp: any = this.fb.group({
    name: [''],
    deptment: [''],
    floor: [''],
    subject: ['Maths']

  });

  //ngOnChanges(changes: SimpleChanges) {
  //  debugger
  //  let chane = changes;
  // }



  dynmicObj: any = this.fb.group({
    aliases: this.fb.array([
      this.fb.control('')
    ])
  })



  get aliases() {

    return this.dynmicObj.get('aliases') as FormArray;
  }

  addAlias() {
    debugger
    this.aliases.push(this.fb.control(''));
  }

  ngOnInit() {
    debugger
  }

  addEmployee() {
    debugger
  }

  InsertEmployee() {
    debugger

    let objArr = this.objEmp.value;
    if (!this.objEmp.valid) {
      this._toaster.warning("Please enter required fields")
    }

  }



}
