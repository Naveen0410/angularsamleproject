import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ppages-component',
  template:


    `
    
      <app-header-component></app-header-component>
      <div style="margin-left:100px;margin-top: 50px;;"> <router-outlet>
      </router-outlet>
      </div>
     
    `
})
export class PagesComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
