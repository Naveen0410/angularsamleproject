import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutingModule } from './pages-routing.module';
import { PagesComponent } from './pages-component.component';
import { PageOneComponentComponent } from './page-one-component/page-one-component.component';
import { PageTwoComponentComponent } from './page-two-component/page-two-component.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms'
import { SharedModuleModule } from '../GlobalFolder/shared-module/shared-module.module';


@NgModule({
  declarations: [PagesComponent, PageOneComponentComponent,
    PageTwoComponentComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    RouterModule,
    ReactiveFormsModule,
    SharedModuleModule
  ]
})
export class PagesModule { }
