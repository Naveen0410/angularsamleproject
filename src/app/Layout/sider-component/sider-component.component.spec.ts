import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SiderComponentComponent } from './sider-component.component';

describe('SiderComponentComponent', () => {
  let component: SiderComponentComponent;
  let fixture: ComponentFixture<SiderComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiderComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SiderComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
